import pytest

from src.config import get_db_settings


@pytest.fixture(scope="session")
def pg_url() -> str:
    settings = get_db_settings()
    return settings.TEST_DATABASE_URI
