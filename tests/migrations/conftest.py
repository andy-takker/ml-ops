import pytest
from sqlalchemy import create_engine

from src.utils.db import alembic_config_from_url, tmp_database


@pytest.fixture()
def postgres(pg_url):
    """Creates empty temporary database."""

    with tmp_database(pg_url, "database") as tmp_url:
        yield tmp_url


@pytest.fixture()
def postgres_engine(postgres):
    """SqlAlchemy engine, bound to temporary database."""
    engine = create_engine(postgres, echo=False)
    try:
        yield engine
    finally:
        engine.dispose()


@pytest.fixture()
def alembic_config(postgres):
    """Alembic configuration object, bound to temporary database."""
    return alembic_config_from_url(postgres)
