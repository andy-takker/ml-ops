from collections.abc import Callable

import pytest
from alembic.command import downgrade, upgrade

from src.utils.db import make_validation_params_groups
from tests.migrations.data_migrations import migration_6af2212215ff


def get_data_migrations():
    return make_validation_params_groups(
        migration_6af2212215ff,
    )


@pytest.mark.parametrize(
    ("rev_base", "rev_head", "on_init", "on_upgrade", "on_downgrade"),
    get_data_migrations(),
)
def test_data_migrations(
    alembic_config,
    postgres_engine,
    rev_base: str,
    rev_head: str,
    on_init: Callable,
    on_upgrade: Callable,
    on_downgrade: Callable,
):
    # Upgrade to previous migration before target and add some data,
    # that would be changed by tested migrations.
    if rev_base != "-1":
        upgrade(alembic_config, rev_base)
    on_init(engine=postgres_engine)

    # Perform upgrade in tested migration.
    # Check that data is migrated correctly in on_upgrade callback

    upgrade(alembic_config, rev_head)
    on_upgrade(engine=postgres_engine)

    # Perform downgrade in tested migration.
    # Check that changes are reverted back using on_downgrade callback
    downgrade(alembic_config, rev_base)
    on_downgrade(engine=postgres_engine)
