from src.utils.db import load_migration_as_module

migration = load_migration_as_module(
    "migration_2023_05_09_1654_6af2212215ff_init_db.py"
)
rev_base: str = "-1"
rev_head: str = migration.revision


def on_init(engine):
    pass


def on_upgrade(engine):
    pass


def on_downgrade(engine):
    pass
