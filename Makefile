ifeq ($(shell test -e '.env' && echo -n yes),yes)
	include .env
endif

args := $(wordlist 2, 100, $(MAKECMDGOALS))
ifndef args
	MESSAGE = "No such command (or you pass two or many targets to ). List of possible commands: make help"
else
	MESSAGE = "Done"
endif

PYTHONPATH = .


HELP_FUN = \
	%help; while(<>){push@{$$help{$$2//'options'}},[$$1,$$3] \
	if/^([\w-_]+)\s*:.*\#\#(?:@(\w+))?\s(.*)$$/}; \
	print"$$_:\n", map"  $$_->[0]".(" "x(20-length($$_->[0])))."$$_->[1]\n",\
    @{$$help{$$_}},"\n" for keys %help; \


# Commands

help: ##@Help Show this help
	@echo -e "Usage: make [target] ...\n"
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)


env: ##@Environment Create .env file with variables
	@$(eval SHELL:=/bin/bash)
	@cp .env.dev .env
	@echo "POSTGRES_PASSWORD=$$(openssl rand -hex 16)" >> .env
	@echo "REDIS_PASSWORD=$$(openssl rand -hex 16)" >> .env

db: ##@Database Create database with docker-compose
	docker-compose -f docker-compose.yml up -d --remove-orphans

revision: ##@Database Create new revision file automatically with prefix
	cd src/db

migrate: ##@Database Make migration
	echo "Will be"

parse_data:
	PYTHONPATH=. poetry run



%::
	echo $(MESSAGE)