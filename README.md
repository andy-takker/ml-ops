# Academy A LMS

GetCourse: https://academiaa.getcourse.ru

## Архитектура

### Компонентная диаграмма

```plantuml
@startuml

database "PostgreSQL" as db <<Database>>
queue [Redis] as broker <<Message Broker>>
component [Nginx] as proxy <<Proxy Server>>
node "LMS" as lms {
    component [Flask] as back <<Application>>
    component [Celery] as scheduler <<Scheduler>>
    component [Default (Celery)] as d_worker <<Worker>>
    component [CRM (Celery)] as crm_worker <<Worker>>
    scheduler <-> back
    d_worker <--> db
    crm_worker <--> db
}

proxy <--> back
back --> broker
scheduler --> broker
d_worker <--> broker
crm_worker <--> broker
scheduler <-> db
@enduml
```
 


### Технологии

- Backend - Python 3.11, Flask, SqlAlchemy
- UI - Flask Admin
- Database - PostgreSQL
- Message Broker - Redis
- Cache - Redis
- Proxy - Nginx

### Схема базы данных

```plantuml
@startuml

entity cohort {
    * id: integer
    ---
    * created_at: datetime
    * updated_at: datetime

    * name: varchar(64)
}

entity course {
    * id: integer
    ---
    * created_at: datetime
    * updated_at: datetime

    * name: varchar(128)
    * short_name: varchar(64)
    * has_stepik_course: boolean
    * stepik_id: integer
    * is_graded: boolean
    * subject_id: integer
    linked_course_id: integer
}

entity offer {
    * id: integer
    ---
    * created_at: datetime
    * updated_at: datetime

    * name: varchar(512)
    * course_id: integer
    * package_id: integer
    * product_id: integer
    * cohort_id: integer
    * process_type: varchar(32)
    * price: float
    * access_end_date: date

    * archived: boolean
}

entity deleted_data {
    * id: integer
    ---
    * user_id: integer
    * data: varchar

    * created_at: datetime
    * updated_at: datetime
}

entity package {
    * id: integer
    ---
    * name: string
    * has_curator: boolean
    * has_mentor: boolean

    * created_at: datetime
    * updated_at: datetime
}

entity product_course {
    * id: integer
    ---
    * product_id: integer
    * course_id: integer
}

entity product_group {
    * id: integer
    ---
    * name: varchar(128)
    * eng_name: varchar(128)

    * created_at: datetime
    * updated_at: datetime
}

entity product {
    * id: integer
    ---
    * name: varchar(512)
    * subject_id: integer
    * check_spreadsheet_id: varchar(256)
    * drive_folder_id: varchar(256)
    * start_date: date
    * end_date: date
    * grade_calculation_date: date
    * product_group_id: integer
    * has_report: boolean
    * main_check_folder_ids: varchar
    * additional_check_folder_ids: varchar

    * archived: boolean

    * created_at: datetime
    * updated_at: datetime
}

entity reviewer {
    id: integer
    ---
    * mail: varchar(64)
    * desired_students: integer
    * max_students: integer
    * abs_max_students: integer
    * teacher_product_id: integer
    * subject_id: integer

    * created_at: datetime
    * updated_at: datetime

    * archived: boolean
}

entity score {
    * id: integer
    ---
    * score: float
    * max_score: float
    * rated_at: date
    * student_course_id: integer
    * student_product_id: integer
    * product_id: int
    * warning: boolean
    previous_score_id: integer
    * message_id: integer

    * created_at: datetime
    * updated_at: datetime
}

entity setting {
    * id: integer
    ---
    * created_at: datetime
    * updated_at: datetime

    * key: varchar(256)
    * type: varchar(32)
    * value: varchar
    description: varchar
}

entity stepik {
    * id: integer
    ---
    * created_at: datetime
    * updated_at: datetime

    * getcourse_id: integer <<FK>>
    * mail: varchar(64)
    * password: varchar(64)
    old_id: integer
}
entity student_course {
    * id: integer
    ---
    * student_id: integer
    * student_product_id: integer
    * course_id: integer
    * regsitration_date: datetime
    * has_access: boolean
    * unverified_tasks: varchar
    * expulsion_date: datetime
    * group: varchar
    * group_id: integer
    linked_student_course_id: integer

    * archived: boolean
}

entity student_product {
    * id: integer
    ---
    * student_id: integer
    * product_id: integer
    * cohort_id: integer
    * offer_id: integer  
    curator_id: integer
    mentor_id: integer

    * created_at: datetime
    * updated_at: datetime

    * archived: boolean
}

entity student {
    * getcourse_id: integer
    ---
    * getcourse_mail: varchar(64)
    * vk_id: integer

    * created_at: datetime
    * updated_at: datetime

    * first_name: varchar(64)
    * last_name: varchar(64)
}

entity subject {
    * id: integer
    ---
    * name: varchar(64)
    * eng_name: varchar(64)
    task_name: varchar(128)
    * autopilot_url: varchar(512)
    excluded_sections: varchar
    * crm_spreadsheet_id: varchar(256)
    * group_vk_link: varhcar(1024)

    * created_at: datetime
    * updated_at: datetime

}

entity teacher_assignment{
    * id: integer
    ---
    * student_product_id: integer
    * teacher_product_id: integer
    * assignment_date: datetime
    * removal_date: datetime
    * product_id: integer

    * created_at: datetime
    * updated_at: datetime
}

entity teacher_product_excluded_courses {
    * id: integer
    ---
    * teacher_product_id: integer
    * course_id: integer
}

entity teacher_product {
    * id: integer
    ---
    * teacher_id: integer
    * product_id: integer
    * is_active: boolean
    * is_curator: boolean
    * is_mentor: boolean
    * max_students: integer
    * average_rate: float
    * rate_counter: integer

    * created_at: datetime
    * updated_at: datetime

    * archived: boolean
}

entity teacher {
    * id: integer
    ---
    * getcourse_id: integer
    * getcourse_mail: varchar(64)
    * vk_id: integer

    * created_at: datetime
    * updated_at: datetime

    * first_name: varchar(64)
    * last_name: varchar(64)
}

entity user {
    * id: integer
    ---
    * created_at: datetime
    * updated_at: datetime

    * login: varchar(64)
    * password_hash: varchar(256)

    * first_name: varchar(64)
    * last_name: varchar(64)
}

entity verified_work {
    * id: integer
    ---
    * created_at: datetime
    * updated_at: datetime

    * name: varchar(512)
    * file_id: varchar(128)
    * file_url: varchar(1024)
    student_id: integer
    subject_id: integer
    offer_id: integer
    student_product_id: integer
}

entity work_notification {
    * id: integer
    ---
    * verified_work_id: integer
    * error_message: varchar(512)
    * notificator: varchar(128)
    * is_processed: boolean
    * is_delivered: boolean
}

student  ||--|| stepik


@enduml
```

## Локальный запуск

Для запуска в `docker-compose` необходимо создать файл `.env` и определить в нем
следующие переменные окружения из `lms.dev.env`:

```dotenv
ENV=development         # параметр конфигурации приложения development, production, testing

APP_PORT=5000           # Порт на котором запускать приложение

LMS_POSTGRES_HOST=host                  # хост БД
LMS_POSTGRES_DB=db_name                 # название БД Postgres
LMS_POSTGRES_USER=user                  # пользователь БД Postgres
LMS_POSTGRES_PASSWORD=password          # пароль пользователя БД Postgres
LMS_POSTGRES_PORT=port                  # порт БД Postgres

REDIS_HOST=host                         # хост Redis
REDIS_PORT=port                         # порт Redis
REDIS_PASSWORD=password                 # пароль Redis

CELERY_BROKER_URL=redis://host:port/0    # БД брокера для Celery
RESULT_BACKEND=redis://host:port/0       # БД для результата очереди Celery

JWT_KEY=secret                              # секрет для внешнего токена сервера
SECRET_KEY=secret                           # секрет для сессий логинов

TELEGRAM_BOT_TOKEN=secret                   # токен бота для логов Telegram
TELEGRAM_GROUP_ID=000000                    # ID группы для отправки логов
```


