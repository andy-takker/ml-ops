from celery import current_app as celery_app

from src.controllers.parser.mos_ru import parse_last_articles


@celery_app.task(name="download_last_articles_mos_ru")
def download_last_articles_mos_ru() -> None:
    parse_last_articles(with_db=True)
