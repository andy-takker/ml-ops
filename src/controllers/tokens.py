import logging
import random
from pathlib import Path

import pandas as pd

logger = logging.getLogger(__name__)

DEFAULT_PATH = Path("./data") / "token_clusters.csv"


class Tokens:
    def __init__(self, filepath: Path):
        self.filepath = filepath
        self.df = self.load_tokens()
        self.mapping_by_cluster = self.create_mapping_by_cluster()
        self.mapping_by_token_id = self.create_mapping_by_token_id()

    def load_tokens(self) -> pd.DataFrame:
        return pd.read_csv(self.filepath)

    def create_mapping_by_cluster(self) -> dict[int, list[int]]:
        mapping_by_cluster = dict()
        for cluster, group in self.df.groupby("cluster"):
            mapping_by_cluster[cluster] = list(group.token_id)

        return mapping_by_cluster  # type: ignore

    def create_mapping_by_token_id(self) -> dict[int, int]:
        mapping_by_token_id = dict()
        for token_id, cluster in zip(self.df.token_id, self.df.cluster):
            mapping_by_token_id[token_id] = cluster

        return mapping_by_token_id

    def get_cluster(self, token_id: int) -> int:
        return self.mapping_by_token_id[token_id]

    def get_token_id_from_cluster(self, cluster: int) -> int:
        return random.choice(self.mapping_by_cluster[cluster])

    def get_random_token(self, token_id: int) -> int:
        cluster = self.get_cluster(token_id)
        random_token = self.get_token_id_from_cluster(cluster)
        return random_token


if __name__ == "__main__":
    tokens = Tokens(filepath=DEFAULT_PATH)
    logger.info("Random token for token 1550 is", tokens.get_random_token(1550))
