import logging
from pathlib import Path

import pandas

logger = logging.getLogger(__name__)
logger.setLevel("INFO")


def filter_article_data(input_file: Path, output_file: Path) -> None:
    """Filter short articles from dataframe"""
    if not input_file.exists():
        raise ValueError("Input File doesn't exist!")
    df = pandas.read_csv(input_file)
    logging.info("Load `%s` with %d records", input_file, df.shape[0])
    df["content"] = df["content"].astype("str")
    df = df.loc[df["content"].str.len() > 1000]
    df.to_csv(output_file)
    logging.info("Save output in `%s` with %d records", output_file, df.shape[0])
