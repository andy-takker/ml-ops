import logging
from functools import partial
from pathlib import Path
from typing import Any

import numpy as np
import pandas
import torch
from transformers import AutoModel, AutoTokenizer

from src.config import get_app_settings

logger = logging.getLogger(__name__)
logger.setLevel("INFO")

# Load model from HuggingFace Hub
tokenizer: AutoTokenizer | None = None
model: AutoModel | None = None


def load_models() -> tuple[Any | None, Any | None]:
    settings = get_app_settings()
    pretrained_model = "sentence-transformers/paraphrase-multilingual-mpnet-base-v2"
    try:
        tokenizer = AutoTokenizer.from_pretrained(
            pretrained_model_name_or_path=pretrained_model,
            cache_dir=settings.model_cache_dir,
        )
        model = AutoModel.from_pretrained(
            pretrained_model_name_or_path=pretrained_model,
            cache_dir=settings.model_cache_dir,
        )
        return tokenizer, model
    except Exception:
        pass
    return None, None


# Mean Pooling - Take attention mask into account for correct averaging
def mean_pooling(model_output: Any, attention_mask: Any) -> torch.Tensor:
    # First element of model_output contains all token embeddings
    token_embeddings = model_output[0]
    input_mask_expanded = (
        attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
    )
    return torch.sum(token_embeddings * input_mask_expanded, 1) / torch.clamp(
        input_mask_expanded.sum(1), min=1e-9
    )


def get_vector(text: str, tokenizer: Any, model: Any) -> np.ndarray:
    if tokenizer is None or model is None:
        raise ValueError
    # Tokenize sentences
    encoded_input = tokenizer(text, padding=True, truncation=True, return_tensors="pt")
    # Compute token embeddings
    with torch.no_grad():
        model_output = model(**encoded_input)

    # Perform pooling. In this case, max pooling.
    sentence_embeddings = mean_pooling(model_output, encoded_input["attention_mask"])[0]
    return sentence_embeddings.cpu().numpy().tolist()


def vectorize_article_data(input_file: Path, output_file: Path) -> None:
    """Vectorize article contents from dataframe"""
    if not input_file.exists():
        raise ValueError("Input File doesn't exist!")
    tokenizer, model = load_models()
    if model is None or tokenizer is None:
        raise ValueError("Model or Tokenizer is None!")
    _get_vector = partial(get_vector, model=model, tokenizer=tokenizer)
    df = pandas.read_csv(input_file)
    logging.info("Load `%s` with %d records", input_file, df.shape[0])

    df["vector"] = df["content"].apply(_get_vector)  # type: ignore
    df.drop(columns=["content"], inplace=True)
    df.to_csv(output_file)
    logging.info("Save output in `%s` with %d records", output_file, df.shape[0])
