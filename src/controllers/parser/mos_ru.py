import logging
import queue
from datetime import datetime, timedelta
from pathlib import Path
from threading import Thread
from typing import ClassVar

import orjson
import requests
from pandas import DataFrame
from requests.adapters import HTTPAdapter, Retry
from sqlalchemy.orm import Session

from src.controllers.vectorization import get_vector, load_models
from src.db.engine import Session as SessionLocal
from src.db.models import Article, ArticleVector
from src.routers.schemas import ArticleData
from src.utils.html_template import clean_text

logger = logging.getLogger(__name__)
logger.setLevel("INFO")


class MosRuParser:
    articles: list[str]
    URL: ClassVar[str] = "https://www.mos.ru/api/newsfeed/v4/frontend/json/ru/articles"

    def __init__(
        self,
        session: Session | None = None,
        df_filename: Path | None = None,
        url: str = URL,
        source_id: int = 1,
        workers: int = 5,
    ):
        self.url = url
        self.source_id = source_id
        self.session = session
        self.df_filename = df_filename

        self.workers = workers

        retries = Retry(
            total=5, backoff_factor=0.1, status_forcelist=[403, 405, 500, 502, 503, 504]
        )
        self.req_session = requests.Session()
        self.req_session.mount("http://", HTTPAdapter(max_retries=retries))
        self.req_session.mount("https://", HTTPAdapter(max_retries=retries))

    def get_article_text(self, id: int | str) -> str | None:
        try:
            response = self.req_session.get(f"{self.url}/{id}")
            if response.ok:
                return clean_text(response.json()["full_text"])
        except:
            pass
        return None

    def parse_page(
        self,
        q: queue.Queue,
        results: list[ArticleData],
        date_from: datetime,
        date_to: datetime,
    ) -> None:
        """Run parsing page in thread"""
        while True:
            try:
                page = q.get_nowait()
            except Exception:
                break
            if not page:
                break
            articles_data = []
            data = self.get_article_ids(
                page=page,
                date_from=date_from.strftime("%Y-%m-%d %H:%M:%S"),
                date_to=date_to.strftime("%Y-%m-%d %H:%M:%S"),
            )
            if data is not None:
                for item in data["items"]:
                    external_id = str(item["id"])
                    content = self.get_article_text(external_id)
                    if content is not None:
                        articles_data.append(
                            ArticleData(
                                external_id=external_id,
                                source_id=self.source_id,
                                content=content,
                            )
                        )
                results.extend(articles_data)
                logger.info("Parsed %d page", page)
            q.task_done()

    def parse_articles(
        self,
        date_from: datetime,
        date_to: datetime | None = None,
    ) -> None:
        articles_data: list[ArticleData] = []
        if date_to is None:
            date_to = datetime.now()
        logger.info(
            "Start parsing from %s, to %s",
            date_from.strftime("%d.%m.%Y"),
            date_to.strftime("%d.%m.%Y"),
        )
        fp_data = self.get_article_ids(
            page=1,
            date_from=date_from.strftime("%Y-%m-%d %H:%M:%S"),
            date_to=date_to.strftime("%Y-%m-%d %H:%M:%S"),
        )
        if fp_data is None:
            raise ValueError("Can't get data!")
        total_pages = fp_data["_meta"]["pageCount"]
        logger.info("Prepared parsing %d pages", total_pages)
        q: queue.Queue = queue.Queue()
        for i in range(total_pages):
            q.put(i + 1)
        logger.info("Init queue")
        threads = [
            Thread(
                target=self.parse_page,
                name=f"thread_{i}",
                args=(q, articles_data, date_from, date_to),
            )
            for i in range(self.workers)
        ]
        for th in threads:
            th.start()
        q.join()
        logger.info("All pages are parsed!")
        if self.session is not None:
            self.save_articles_in_db(articles_data=articles_data)
        else:
            self.save_articles_in_csv(articles_data=articles_data)
        return None

    def get_article_ids(
        self, date_from: str = "", date_to: str = "", page: int = 1
    ) -> dict | None:
        try:
            response = self.req_session.get(
                self.url,
                params={  # type: ignore
                    "fields": "id",
                    "from": date_from,
                    "to": date_to,
                    "per-page": 50,
                    "page": page,
                    "sort": "date",
                },
            )
            if response.ok:
                return response.json()
            logger.error(response.status_code)
        except:
            pass
        return None

    def save_articles_in_db(self, articles_data: list[ArticleData]) -> None:
        assert self.session is not None
        tokenizer, model = load_models()
        for article_data in articles_data:
            if (
                self.session.query(Article)
                .filter_by(
                    external_id=article_data.external_id, source_id=self.source_id
                )
                .exists()
            ):
                continue
            article = Article(**article_data.dict())
            vector = orjson.dumps(
                get_vector(text=article_data.content, tokenizer=tokenizer, model=model)
            ).decode()
            a_vector = ArticleVector(article=article, vector=vector)  # type: ignore
            self.session.add_all((article, a_vector))
            self.session.commit()
            logger.info("Added element %s", article_data.external_id)
        logger.info("All articles were saved!")

    def save_articles_in_csv(self, articles_data: list[ArticleData]) -> None:
        """Save articles in csv df_filename"""
        assert self.df_filename is not None
        assert self.df_filename.parent.resolve().exists(), "Parent folder must exist!"

        df = DataFrame.from_dict(  # type: ignore
            article_data.dict() for article_data in articles_data
        )
        df.to_csv(self.df_filename)
        logger.info(
            "Articles saved in %s with %d records", self.df_filename, df.shape[0]
        )


def parse_last_articles(
    days: int = 1, with_db: bool = True, df_filename: Path | None = None
) -> None:
    """Parse articles from MosRu last `n` days"""
    if not with_db and df_filename is None or with_db and df_filename is not None:
        raise ValueError("Need or database mode, or saving in dataframe")
    session = None
    if with_db:
        session = SessionLocal()
    date_from = datetime.now() - timedelta(days=days)
    MosRuParser(session=session, df_filename=df_filename).parse_articles(
        date_from=date_from
    )


def main() -> None:
    parse_last_articles(days=270)


if __name__ == "__main__":
    main()
