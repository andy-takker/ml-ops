import logging
from collections.abc import Generator, Sequence
from dataclasses import dataclass
from typing import Any

import numpy as np
import orjson
import requests
from nltk import ngrams

from src.controllers import sample_articles_content, vectorization

logger = logging.getLogger(__name__)

NGRAM_LENGTH = 12
THRESHOLD_SIMILARITY_MIN_FOR_NGRAM = -1


def get_cosine_similarity(v1: np.ndarray, v2: np.ndarray) -> float:
    """Рассчитывает косинусную близость"""
    return np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))


class Ngram:
    url_translate = "https://morescience.app/api/translate"
    url_sentiment = "https://morescience.app/api/sentiment"

    def __init__(self, ngram: list[str]):
        self.ngram_lst = ngram
        self.ngram_str = " ".join(ngram)
        self.vector = self.vectorize()
        self.translated: str | None = None
        self.ner_sentiment: dict | None = None
        self.entities: list | None = None

    def vectorize(self) -> np.ndarray:
        tokenizer, model = vectorization.load_models()
        return vectorization.get_vector(
            self.ngram_str, tokenizer=tokenizer, model=model
        )

    def get_translate(self) -> None:
        """Получаем данные через прокси из Google Natural Language API"""
        r = requests.post(self.url_translate, json={"string": self.ngram_str})
        translated = r.text or ""
        self.translated = translated

    def get_ner_sentiment(self) -> None:
        """Получаем данные через прокси из Google Natural Language API"""
        r = requests.post(self.url_sentiment, json={"string": self.translated})
        self.ner_sentiment = orjson.loads(r.text)

    def get_entities(self) -> None:
        entities = list()
        if self.ner_sentiment is not None:
            for entity in self.ner_sentiment["entities"]:
                entities.append(
                    {"name": entity["name"], "salience": entity["salience"]}
                )

        self.entities = entities

    def __repr__(self) -> str:
        return self.ngram_str

    def __str__(self) -> str:
        return self.ngram_str


class ArticleNer:
    def __init__(self, text: str, type_: bool, ngram_length: int = NGRAM_LENGTH):
        self.text = text
        self.type = type_  # true or false
        logger.info(f"type = {self.type}")
        self.list = text.split()
        self.ngram_length = ngram_length
        if self.type is True:
            self.ngrams = self.split_to_ngrams_true()
            logger.info(f"ngrams = {self.ngrams}")
        else:
            self.ngrams = self.split_to_ngrams_false()
            logger.info(f"ngrams = {self.ngrams}")

    def get_chunks(
        self, lst: list[str], ngram_length: int
    ) -> Generator[list[str], None, None]:
        """Yield successive n-sized chunks from lst."""
        for i in range(0, len(self.list), ngram_length):
            yield lst[i : i + ngram_length]

    def split_to_ngrams_true(self) -> list[Ngram]:
        ngrams_ = ngrams(self.list, self.ngram_length)
        ngrams_ = [Ngram(i) for i in ngrams_]
        return ngrams_

    def split_to_ngrams_false(self) -> list[Ngram]:
        ngrams_ = [Ngram(i) for i in self.get_chunks(self.list, self.ngram_length)]
        return ngrams_


@dataclass
class NgramPair:
    ngram_true: Ngram
    ngram_false: Ngram
    status: str

    def print(self) -> None:
        print(
            f"ngram_false = {self.ngram_false.ngram_str}\n"
            f"gram_true = {self.ngram_true.ngram_str}\n\n"
        )


class ArticlePair:
    def __init__(self, article_1: ArticleNer, article_2: ArticleNer):
        if article_1.type:
            self.article_true = article_1
            self.article_false = article_2
        else:
            self.article_true = article_2
            self.article_false = article_1

        self.ngram_pairs = self.get_ngram_pairs()
        self.result = self.compare_pairs()

    def print(self) -> None:
        for ngram in self.ngram_pairs:
            ngram.print()
        return

    def get_closest_ngram(self, ngram_false: Ngram) -> tuple[Ngram, str]:
        similarities = dict()
        for ngram_true in self.article_true.ngrams:
            similarities[
                get_cosine_similarity(ngram_false.vector, ngram_true.vector)
            ] = ngram_true

        max_similarity = max(similarities.keys())
        if max_similarity < THRESHOLD_SIMILARITY_MIN_FOR_NGRAM:
            return ngram_false, "not found"
        return similarities[max_similarity], "found"

    def get_ngram_pairs(self) -> list[NgramPair]:
        ngram_pairs = list()
        for ngram_false in self.article_false.ngrams:
            ngram_true_closest, status = self.get_closest_ngram(ngram_false)
            ngram_pair = NgramPair(
                ngram_true=ngram_true_closest, ngram_false=ngram_false, status=status
            )
            ngram_pairs.append(ngram_pair)

        return ngram_pairs

    def get_average_weighted(
        self, values: Sequence[Any], weights: Sequence[Any]
    ) -> float:
        return np.average(values, weights=weights)

    def compare_pairs(self) -> dict[str, Any]:
        result = list()
        saliences = list()
        for ngram_pair in self.ngram_pairs:
            ngram_pair.ngram_true.get_translate()
            ngram_pair.ngram_false.get_translate()

            ngram_pair.ngram_true.get_ner_sentiment()
            ngram_pair.ngram_false.get_ner_sentiment()

            ngram_pair.ngram_true.get_entities()
            ngram_pair.ngram_false.get_entities()

            ner_sentiment_entities_true = ngram_pair.ngram_true.entities or []
            ner_sentiment_entities_false = ngram_pair.ngram_false.entities or []

            ner_sentiment_entities_true_names = [
                i["name"] for i in ner_sentiment_entities_true
            ]
            ner_sentiment_entities_false_names = [
                i["name"] for i in ner_sentiment_entities_false
            ]

            ner_sentiment_entities_true_salience = [
                i["salience"] for i in ner_sentiment_entities_true
            ]
            ner_sentiment_entities_false_salience = [
                i["salience"] for i in ner_sentiment_entities_false
            ]

            salience = np.median(
                ner_sentiment_entities_true_salience
                + ner_sentiment_entities_false_salience
            )
            saliences.append(salience)

            intersection = set(ner_sentiment_entities_true_names) & set(
                ner_sentiment_entities_false_names
            )
            coef = len(intersection) / len(ner_sentiment_entities_true)

            text_true = ngram_pair.ngram_true.ngram_str
            if ngram_pair.status == "not found":
                text_true = ""

            # if ngram_pair.status == 'found':
            #     print()

            result.append(
                {
                    "text_false": ngram_pair.ngram_false.ngram_str,
                    "text_true": text_true,
                    "truth": coef,
                }
            )

        # percent = np.median([i['truth'] for i in result])
        percent = self.get_average_weighted([i["truth"] for i in result], saliences)

        return {"ngrams": result, "percentage": percent}


def main() -> None:
    article_true = ArticleNer(sample_articles_content.article_true_string, type_=True)
    article_false = ArticleNer(
        sample_articles_content.article_false_string, type_=False
    )

    article_pair = ArticlePair(article_true, article_false)
    logger.info(article_pair.result)


if __name__ == "__main__":
    main()
