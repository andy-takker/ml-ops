from pathlib import Path

import click

from src.controllers.filters import filter_article_data
from src.controllers.parser.mos_ru import parse_last_articles
from src.controllers.vectorization import vectorize_article_data


@click.group()
def pipeline() -> None:
    pass


@pipeline.command("parse_data")
@click.option("--output-file", "output_file", type=click.Path(), required=True)
@click.option("--days", "days", type=click.INT, default=1)
def parse_data(output_file: str, days: int = 1) -> None:
    parse_last_articles(days=days, with_db=False, df_filename=Path(output_file))


@pipeline.command("filter_data")
@click.option("--input-file", "input_file", type=click.Path(exists=True), required=True)
@click.option("--output-file", "output_file", type=click.Path(), required=True)
def filter_data(input_file: str, output_file: str) -> None:
    filter_article_data(input_file=Path(input_file), output_file=Path(output_file))


@pipeline.command("vectorize_data")
@click.option("--input-file", "input_file", type=click.Path(exists=True), required=True)
@click.option("--output-file", "output_file", type=click.Path(), required=True)
def vectorize_data(input_file: str, output_file: str) -> None:
    vectorize_article_data(input_file=Path(input_file), output_file=Path(output_file))


if __name__ == "__main__":
    pipeline()
