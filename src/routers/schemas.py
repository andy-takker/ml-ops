from __future__ import annotations

from pydantic import HttpUrl

from src.utils import BaseModel


class SourceBase(BaseModel):
    title: str
    description: str | None
    base_url: HttpUrl


class Source(SourceBase):
    id: int

    articles: list[Article] = []

    class Config:
        orm_mode = True


class ArticleBase(BaseModel):
    content: str


class Article(ArticleBase):
    source_id: int

    source: Source
    vector: ArticleVector
    suspicious_articles: list[SuspiciousArticleBase] = []

    class Config:
        orm_mode = True


class SuspiciousArticleBase(BaseModel):
    content: str
    is_true: bool | None
    percentage: float | None
    result: str | None


class SuspiciousArticle(SuspiciousArticleBase):
    article_id: int | None

    article: Article | None

    class Config:
        orm_mode = True


class NgramPairCoef(BaseModel):
    false_text: str
    truth: float
    true_text: str


class Answer(BaseModel):
    suspicious_content: str
    percentage: float
    article: str
    result: list[NgramPairCoef]
    source: str


class ArcticleVectorBase(BaseModel):
    vector: str


class ArticleVectorOutput(BaseModel):
    vector: list[float]


class ArticleVector(ArcticleVectorBase):
    article_id: int

    article: Article

    class Config:
        orm_mode = True


Source.update_forward_refs()
Article.update_forward_refs()
SuspiciousArticle.update_forward_refs()
ArticleVector.update_forward_refs()


class ArticleData(BaseModel):
    external_id: str
    source_id: int
    content: str
