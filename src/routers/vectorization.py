from fastapi import APIRouter, Form

from src.controllers.vectorization import get_vector, load_models
from src.routers.schemas import ArticleVectorOutput

router = APIRouter(tags=["vectors"])


@router.post(
    "/common_vectorization", status_code=200, response_model=ArticleVectorOutput
)
async def common_vectorization(text: str = Form(...)) -> ArticleVectorOutput:
    tokenizer, model = load_models()
    return ArticleVectorOutput(
        vector=get_vector(text, tokenizer=tokenizer, model=model)
    )
