import logging

import numpy as np
import orjson
from fastapi import APIRouter, Depends, Request, Response
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session

from src.controllers.article_ner import ArticleNer, ArticlePair, get_cosine_similarity
from src.controllers.vectorization import get_vector, load_models
from src.db.engine import get_session
from src.db.models import Article, ArticleVector, SuspiciousArticle
from src.routers.schemas import Answer, ArticleBase
from src.utils.html_template import clean_text, colorize

logger = logging.getLogger(__name__)

router = APIRouter(tags=["Fake News"])

templates = Jinja2Templates(directory="frontend/templates")


@router.post("/check-article_ner_sentiment", status_code=200, response_model=Answer)
async def check_article_ner_sentiment(
    suspicious_article: ArticleBase, session: Session = Depends(get_session)
) -> Answer:
    suspicious_article_text = clean_text(suspicious_article.content)

    s_article: SuspiciousArticle | None = (
        session.query(SuspiciousArticle)
        .filter_by(content=suspicious_article_text)
        .first()
    )
    if s_article is not None:
        logger.info(orjson.loads(s_article.result))
        return Answer(
            suspicious_content=s_article.content,
            percentage=s_article.percentage,
            article=s_article.article.content,
            result=orjson.loads(s_article.result),
            source=s_article.article.source.title,
        )
    tokenizer, model = load_models()
    suspicious_vector = get_vector(
        suspicious_article_text, tokenizer=tokenizer, model=model
    )
    v_articles: list[ArticleVector] = session.query(ArticleVector).all()
    similarities = []
    for v_article in v_articles:
        sim = get_cosine_similarity(
            np.array(orjson.loads(v_article.vector)), suspicious_vector
        )
        similarities.append((sim, v_article.article_id))
    # logger.info()
    id_ = max(similarities, key=lambda x: x[0])[1]
    article = session.get(Article, id_)
    if article is None:
        raise ValueError("Article doesn't exists!")

    article_true = ArticleNer(article.content, type_=True)
    article_false = ArticleNer(suspicious_article_text, type_=False)

    article_pair = ArticlePair(article_true, article_false)
    ngrams = list(
        map(
            lambda x: {
                "truth": round(x["truth"] * 100),
                "text_true": x["text_true"],
                "text_false": x["text_false"],
            },
            article_pair.result["ngrams"],
        )
    )
    percentage = round(article_pair.result["percentage"] * 100)
    sa = SuspiciousArticle()
    sa.content = suspicious_article_text
    sa.article_id = article.id
    sa.percentage = percentage
    sa.is_true = percentage > 75
    sa.result = orjson.dumps(ngrams).decode()
    session.add(sa)
    session.commit()
    return Answer(
        suspicious_content=suspicious_article_text,
        percentage=percentage,
        article=article.content,
        result=ngrams,
        source=article.source.title,
    )


@router.get("/", response_class=HTMLResponse)
async def check_fake_get(request: Request) -> Response:
    return templates.TemplateResponse(
        "index.html",
        {
            "request": request,
        },
    )


@router.post("/", response_class=HTMLResponse)
async def check_fake_post(
    request: Request, session: Session = Depends(get_session)
) -> Response:
    form = await request.form()
    text = form.get("input_text")
    if not isinstance(text, str):
        raise TypeError("Need string")
    text = clean_text(text)

    s_article: SuspiciousArticle | None = (
        session.query(SuspiciousArticle).filter_by(content=text).first()
    )
    if s_article is not None:
        logger.info(orjson.loads(s_article.result))
        text = colorize(text, orjson.loads(s_article.result))
        return templates.TemplateResponse(
            "index.html",
            {
                "request": request,
                "answer": Answer(
                    suspicious_content=text,
                    percentage=s_article.percentage,
                    article=s_article.article.content,
                    result=orjson.loads(s_article.result),
                    source=s_article.article.source.title,
                ).dict(),
            },
        )
    tokenizer, model = load_models()
    suspicious_vector = get_vector(text, tokenizer=tokenizer, model=model)
    v_articles: list[ArticleVector] = session.query(ArticleVector).all()
    similarities = []
    for v_article in v_articles:
        sim = get_cosine_similarity(
            np.array(orjson.loads(v_article.vector)), suspicious_vector
        )
        similarities.append((sim, v_article.article_id))
    id_ = max(similarities, key=lambda x: x[0])[1]
    article = session.get(Article, id_)
    if article is None:
        raise ValueError("Article doesn't exists!")
    article_true = ArticleNer(article.content, type_=True)
    article_false = ArticleNer(text, type_=False)

    article_pair = ArticlePair(article_true, article_false)
    ngrams = list(
        map(
            lambda x: {
                "truth": round(x["truth"] * 100),
                "text_true": x["text_true"],
                "text_false": x["text_false"],
            },
            article_pair.result["ngrams"],
        )
    )
    percentage = int(round(article_pair.result["percentage"] * 100))
    sa = SuspiciousArticle()
    sa.content = text
    sa.article_id = article.id
    sa.percentage = percentage
    sa.is_true = percentage > 75
    sa.result = orjson.dumps(ngrams).decode()
    session.add(sa)
    session.commit()

    return templates.TemplateResponse(
        "index.html",
        {
            "request": request,
            "answer": Answer(
                suspicious_content=colorize(text, ngrams),
                percentage=percentage,
                article=article.content,
                source=article.source.title,
                result=ngrams,
            ).dict(),
        },
    )
