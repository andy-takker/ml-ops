import logging

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from src.db.engine import get_session
from src.db.models import SuspiciousArticle
from src.routers.schemas import SuspiciousArticle as SuspiciousArticleSchema
from src.routers.schemas import SuspiciousArticleBase

logger = logging.getLogger(__name__)

router = APIRouter(tags=["checking fakes"])


@router.post(
    path="/check-article", status_code=201, response_model=SuspiciousArticleBase
)
def check_article(
    suspicious_article: SuspiciousArticleBase, session: Session = Depends(get_session)
) -> SuspiciousArticleBase:
    logger.info(suspicious_article)
    sa: SuspiciousArticle | None = (
        session.query(SuspiciousArticle)
        .filter(SuspiciousArticle.content == suspicious_article.content)
        .first()
    )
    if sa is not None:
        article = SuspiciousArticleSchema.from_orm(sa)
        return article
    session.add(SuspiciousArticle(**suspicious_article.dict()))
    session.commit()
    suspicious_article.is_true = True
    suspicious_article.percentage = 0.3
    suspicious_article.result = "Test"
    return suspicious_article
