import importlib.machinery
import os
import uuid
from collections import defaultdict, namedtuple
from collections.abc import Generator
from contextlib import contextmanager
from pathlib import Path
from types import ModuleType, SimpleNamespace
from typing import Any

from alembic.config import Config
from configargparse import Namespace
from sqlalchemy_utils import create_database, drop_database

from src.config import get_app_settings

PROJECT_PATH = Path(__file__).parent.parent.parent.resolve()

app_settings = get_app_settings()


def make_alembic_config(
    cmd_opts: Namespace | SimpleNamespace, base_path: Path = PROJECT_PATH
) -> Config:
    if not os.path.isabs(cmd_opts.config):
        cmd_opts.config = base_path / cmd_opts.config

    config = Config(file_=cmd_opts.config, ini_section=cmd_opts.name, cmd_opts=cmd_opts)  # type: ignore

    # Replace path to alembic folder to absolute
    alembic_location = config.get_main_option("script_location") or "migrations/"
    if not os.path.isabs(alembic_location):
        config.set_main_option("script_location", str(base_path / alembic_location))

    if cmd_opts.pg_url:
        config.set_main_option("sqlalchemy.url", cmd_opts.pg_url)

    return config


def alembic_config_from_url(pg_url: str | None = None) -> Config:
    """Provides Python object, representing alebmic.ini file."""
    cmd_options = SimpleNamespace(
        config="alembic.ini",
        name="alembic",
        pg_url=pg_url,
        raiseerr=False,
        x=None,
    )
    return make_alembic_config(cmd_options)


MigrationValidationParamsGroup = namedtuple(
    "MigrationValidationParamsGroup",
    [
        "rev_base",
        "rev_head",
        "on_init",
        "on_upgrade",
        "on_downgrade",
    ],
)


def load_migration_as_module(filename: str) -> ModuleType:
    """Allows to import alembic migration as a module."""
    return importlib.machinery.SourceFileLoader(
        filename,
        str(PROJECT_PATH / "src" / "db" / "migrations" / "versions" / filename),
    ).load_module()


def make_validation_params_groups(*migrations) -> list[MigrationValidationParamsGroup]:  # type: ignore
    """Creates objects that describe test for data migrations."""
    data = []
    for migration in migrations:
        # Ensure migration has all required params
        for required_param in ["rev_base", "rev_head"]:
            if not hasattr(migration, required_param):
                raise RuntimeError(
                    f"{required_param} not specified for {migration.__name__}"
                )

        callbacks = defaultdict(lambda: lambda *args, **kwargs: None)  # type: ignore
        for callback in ["on_init", "on_upgrade", "on_downgrade"]:
            if hasattr(migration, callback):
                callbacks[callback] = getattr(migration, callback)

        data.append(
            MigrationValidationParamsGroup(
                rev_base=migration.rev_base,
                rev_head=migration.rev_head,
                on_init=callbacks["on_init"],
                on_upgrade=callbacks["on_upgrade"],
                on_downgrade=callbacks["on_downgrade"],
            )
        )
    return data


@contextmanager
def tmp_database(
    db_url: str, suffix: str = "", **kwargs: dict[str, Any]
) -> Generator[str, None, None]:
    tmp_db_name = ".".join([uuid.uuid4().hex, app_settings.title, suffix])
    tmp_db_url = f"{db_url.rstrip('/')}/{tmp_db_name}"
    create_database(tmp_db_url, **kwargs)

    try:
        yield tmp_db_url
    finally:
        drop_database(tmp_db_url)
