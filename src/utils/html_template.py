import re
from typing import Any

from markupsafe import Markup


def get_color(percentage: int) -> str:
    if percentage > 75:
        return "light-green"
    if percentage > 50:
        return "yellow"
    if percentage > 25:
        return "orange"
    return "red"


def colorize(text: str, ngrams: list[dict[str, Any]]) -> str:
    for ngram in ngrams:
        part = ngram["text_false"]
        if part and part in text:
            ind = text.find(part)
            text = (
                text[:ind]
                + Markup(
                    f'<span class="{get_color(ngram["truth"])}" title="{ngram["truth"]} {ngram["text_true"]}">'
                )
                + text[ind : ind + len(part)]
                + Markup("</span>")
                + text[ind + len(part) :]
            )

    return text


def clean_text(text: str) -> str:
    return re.sub(r"(&\w*;)|(<[^>]*>)", "", text).strip()
