from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from starlette.staticfiles import StaticFiles
from uvicorn import run

from src.config import get_app_settings
from src.routers.checking_fakes import router as checking_fakes_router
from src.routers.fake_detection_ner_sentiment import router as ner_router
from src.routers.vectorization import router as vectorization_router
from src.utils import get_hostname


def get_application() -> FastAPI:
    settings = get_app_settings()
    application = FastAPI(**settings.fastapi_kwargs)
    application.add_middleware(
        CORSMiddleware,
        allow_origins=settings.allowed_hosts or ["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    application.include_router(router=vectorization_router, prefix="/vectors")
    application.include_router(router=checking_fakes_router, prefix="/checking-fakes")
    application.include_router(router=ner_router, prefix="/ner")

    application.mount(
        "/static", StaticFiles(directory="src/frontend/static"), name="static"
    )
    return application


app = get_application()

if __name__ == "__main__":
    settings = get_app_settings()
    run(
        "src.main:app",
        host=get_hostname(settings.host),
        port=settings.port,
        reload=True,
        reload_dirs=["src"],
        log_level="debug",
    )
