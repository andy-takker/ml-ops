__author__ = "Natalenko Sergey"
__maintainer__ = __author__

__email__ = "sergey.natalenko@mail.ru"
__license__ = "MIT"
__version__ = "0.0.1"

__all__ = (
    "__author__",
    "__email__",
    "__license__",
    "__maintainer__",
    "__version__",
)
