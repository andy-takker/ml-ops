import os
from functools import lru_cache
from pathlib import Path
from typing import Any, TypeVar

from pydantic import BaseSettings, DirectoryPath

DEFAULT_ENV_PATH = "./.env"

SettingsType = TypeVar("SettingsType", bound=BaseSettings)


class AppSettings(BaseSettings):
    debug: bool = False
    docs_url: str = "/docs"
    openapi_prefix: str = ""
    openapi_url: str = "/openapi.json"
    redoc_url: str = "/redoc"
    title: str = "FastAPI"
    description: str = "FastAPI App"
    version: str = "0.1.0"
    static_path: str = "/static"

    disable_docs: bool = False

    host: str = "127.0.0.1"
    port: int = 8000
    allowed_hosts: list[str] | None
    model_cache_dir: DirectoryPath = Path("/tmp")

    @property
    def fastapi_kwargs(self) -> dict[str, Any]:
        fastapi_kwargs: dict[str, Any] = {
            "debug": self.debug,
            "docs_url": self.docs_url,
            "openapi_prefix": self.openapi_prefix,
            "openapi_url": self.openapi_url,
            "redoc_url": self.redoc_url,
            "title": self.title,
            "description": self.description,
            "version": self.version,
        }
        if self.disable_docs:
            fastapi_kwargs.update(
                {"docs_url": None, "openapi_url": None, "redoc_url": None}
            )
        return fastapi_kwargs

    class Config:
        env_prefix = "api_"
        validate_assignment = True


class DBSettings(BaseSettings):
    postgres_user: str
    postgres_password: str
    postgres_host: str
    postgres_db: str
    postgres_port: str

    redis_port: str
    redis_host: str
    redis_password: str

    @property
    def POSTGRES_SUFFIX(self) -> str:
        return f"{self.postgres_user}:{self.postgres_password}@{self.postgres_host}:{self.postgres_port}/{self.postgres_db}"

    @property
    def SYNC_DATABASE_URI(self) -> str:
        return f"postgresql+psycopg2://{self.POSTGRES_SUFFIX}"

    @property
    def TEST_DATABASE_URI(self) -> str:
        return f"postgresql+psycopg2://{self.postgres_user}:{self.postgres_password}@{self.postgres_host}:{self.postgres_port}/"

    @property
    def ASYNC_DATABASE_URI(self) -> str:
        return f"postgresql+asyncpg://{self.POSTGRES_SUFFIX}"

    @property
    def REDIS_URI(self) -> str:
        return f"redis://:{self.redis_password}@{self.redis_host}:{self.redis_port}"


def _get_settings(settings_class: type[SettingsType]) -> SettingsType:
    if os.path.isfile(DEFAULT_ENV_PATH):
        return settings_class(_env_file=DEFAULT_ENV_PATH)
    return settings_class()


@lru_cache
def get_db_settings() -> DBSettings:
    return _get_settings(settings_class=DBSettings)


@lru_cache
def get_app_settings() -> AppSettings:
    return _get_settings(settings_class=AppSettings)
