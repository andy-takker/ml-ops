from __future__ import annotations

from sqlalchemy import Boolean, Float, ForeignKey, String, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship

from src.db.base import Base
from src.db.mixins import TimestampMixin


class Source(TimestampMixin, Base):
    """Data source - element of white list"""

    title: Mapped[str] = mapped_column(String(128), nullable=False)
    description: Mapped[str] = mapped_column(String(512), nullable=True)
    base_url: Mapped[str] = mapped_column(String, nullable=True)

    articles: Mapped[list[Article]] = relationship("Article", back_populates="source")

    def __repr__(self) -> str:
        return f"<Source ({self.title})>"


class Article(TimestampMixin, Base):
    """Article with content"""

    __table_args__ = (
        UniqueConstraint("source_id", "external_id", name="_source_external_uc_"),
    )
    content: Mapped[str] = mapped_column(String, nullable=False)
    source_id: Mapped[int] = mapped_column(
        ForeignKey("source.id"), index=True, nullable=False
    )
    external_id: Mapped[str] = mapped_column(String, nullable=False)

    source: Mapped[Source] = relationship("Source", back_populates="articles")
    vector: Mapped[ArticleVector] = relationship(
        "ArticleVector", back_populates="article", uselist=False
    )
    suspicious_articles: Mapped[list[SuspiciousArticle]] = relationship(
        "SuspiciousArticle", back_populates="article"
    )

    def __repr__(self) -> str:
        return f"<Article (`{self.content[:20]}...`)>"


class SuspiciousArticle(TimestampMixin, Base):
    """Article sent for verififcation"""

    content: Mapped[str] = mapped_column(String, nullable=False)
    article_id: Mapped[int] = mapped_column(
        ForeignKey("article.id"), index=True, nullable=True
    )
    is_true: Mapped[bool] = mapped_column(Boolean)
    percentage: Mapped[float] = mapped_column(Float)
    result: Mapped[str] = mapped_column(String)  # json.dumps

    article: Mapped[Article] = relationship(
        "Article", back_populates="suspicious_articles"
    )

    def __repr__(self) -> str:
        return f"<SuspiciousArticle ({self.is_true =}, {self.percentage = })>"


class ArticleVector(TimestampMixin, Base):
    """Tokenized Article"""

    vector: Mapped[str] = mapped_column(String, nullable=False)  # json.dumps
    article_id: Mapped[int] = mapped_column(
        ForeignKey("article.id"), index=True, nullable=False, unique=True
    )

    article: Mapped[Article] = relationship("Article", back_populates="vector")

    def __repr__(self) -> str:
        return f"<ArticleVector ({self.article_id = })>"
