from collections.abc import Generator

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from src.config import get_db_settings
from src.db.base import Base

settings = get_db_settings()
engine = create_engine(url=settings.SYNC_DATABASE_URI)

session_maker = sessionmaker(autoflush=False, autocommit=False, bind=engine)


def get_session() -> Generator[Session, None, None]:
    """Для DI"""
    session = None
    try:
        session = session_maker()
        yield session
    finally:
        if session is not None:
            session.close()


def create_db() -> None:
    Base.metadata.create_all(engine)
